import { makeObservable, observable, action, computed } from "mobx";
import { ErrorApi } from "src/types";
import User from "./User";

const user = new User("cristhianmenjiar95@gmail.com", "Cristhian", "Menjivar");
const user2 = new User("diego@gmail.com", "Diego", "Avelar");
const user3 = new User("pablo@gmail.com", "Pablo", "Ochoa");
const user4 = new User("jonis@gmail.com", "Jonis", "Menjivar");
const user5 = new User("cindy@gmail.com", "Cindy", "Menjivar");
const user6 = new User("elida@gmail.com", "Elida", "Menjivar");
const user7 = new User("otro@gmail.com", "Otro", "Menjivar");

export class UserStore {
  users: User[] = [user, user2, user3, user4, user5, user6, user7];
  idsUserSelected: string[] = [];
  isModalNewOpen: boolean = false;
  isDialogDeleteConfirm: boolean = false;

  constructor() {
    makeObservable(this, {
      users: observable,
      idsUserSelected: observable,
      isModalNewOpen: observable,
      isDialogDeleteConfirm: observable,
      addUser: action,
      deleteUser: action,
      deleteUsersByIds: action,
      toggleModal: action,
      toggleDialogDelete: action,
      totalEmailVerified: computed,
    });
  }

  addUser = (user: User): User | ErrorApi => {
    try {
      this.users = [user, ...this.users];
      this.toggleModal();
      return user;
    } catch (error) {
      return {
        message: `Error: The new user could not be added, ${error}`,
      };
    }
  };

  deleteUser = (user: User) => {
    this.users = this.users.filter((u) => u.accountId !== user.accountId);
  };

  deleteUsersByIds = () => {
    const isIncludeId = (id: string): boolean =>
      this.idsUserSelected.includes(id);

    this.users = this.users.filter((u) => !isIncludeId(u.accountId));
    this.toggleDialogDelete();
    this.idsUserSelected = [];
  };

  toggleModal = () => {
    if (!this.isModalNewOpen) {
      this.idsUserSelected = [];
    }
    this.isModalNewOpen = !this.isModalNewOpen;
  };

  toggleDialogDelete = () => {
    this.isDialogDeleteConfirm = !this.isDialogDeleteConfirm;
  };

  get totalEmailVerified() {
    return this.users.filter((u) => u.emailVerified).length;
  }
}
