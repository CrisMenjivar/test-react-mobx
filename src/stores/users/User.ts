import { makeObservable, observable, action, computed } from "mobx";
import { uuid } from "uuidv4";

console.log(uuid());

class User {
  accountId: string = uuid();
  username: string = "";
  emailVerified: boolean = false;
  firstName: string = "";
  lastName: string = "";
  createAt: Date = new Date();

  constructor(username?: string, firstName?: string, lastName?: string) {
    makeObservable(this, {
      accountId: observable,
      username: observable,
      emailVerified: observable,
      firstName: observable,
      lastName: observable,
      createAt: observable,

      setEmailVerified: action,
      fullName: computed,
    });

    this.username = username || this.username;
    this.firstName = firstName || this.firstName;
    this.lastName = lastName || this.lastName;
  }

  setEmailVerified = () => {
    this.emailVerified = true;
  };

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}

export default User;
