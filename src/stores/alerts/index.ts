import { Color } from "@material-ui/lab";
import { makeObservable, observable, action } from "mobx";
import { uuid } from "uuidv4";

export class AlertMessage {
  id: string = uuid();
  message: string = "";
  severity: Color = "success";
  color: Color = "info";
  duration: number = 3000;

  constructor(
    message: string,
    severity?: Color,
    color?: Color,
    duration?: number
  ) {
    makeObservable(this, {
      message: observable,
      severity: observable,
      color: observable,
    });

    this.message = message;
    this.severity = severity || this.severity;
    this.color = color || this.color;
    this.duration = duration || this.duration;
  }
}

export class AlertStore {
  alerts: AlertMessage[] = [];

  constructor() {
    makeObservable(this, {
      alerts: observable,
      showMessage: action,
      deleteMessage: action,
    });
  }

  showMessage = (alert: AlertMessage) => {
    this.alerts.push(alert);
  };

  deleteMessage = (alert: AlertMessage) => {
    this.alerts = this.alerts.filter((a) => a.id !== alert.id);
  };
}
