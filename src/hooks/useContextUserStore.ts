import { useContext } from "react";
import UserContext from "../context/UserContext";

const useContextUserStore = () => {
  return useContext(UserContext);
};

export default useContextUserStore;
