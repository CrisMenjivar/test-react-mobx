export interface RowTable {
  accountId: string;
  userName: string;
  emailVerified: boolean;
  firstName: string;
  lastName: string;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof RowTable;
  label: string;
  numeric: boolean;
}

export interface ErrorApi {
  message: string;
}

export type Order = "asc" | "desc";
