import { createContext } from "react";
import { UserStore } from "../stores/users/UsersStore";

type UserContextProps = {
  userStore: UserStore | null;
};

const defaultContext: UserContextProps = {
  userStore: null,
};

const UserContext = createContext<UserContextProps>(defaultContext);

export default UserContext;
