import { createContext } from "react";
import { AlertStore } from "../stores/alerts";

type AlertContextProps = {
  alertStore: AlertStore | null;
};

const defaultContext: AlertContextProps = {
  alertStore: null,
};

const AlertContext = createContext<AlertContextProps>(defaultContext);

export default AlertContext;
