import React from "react";
import { Container, createStyles, makeStyles, Theme } from "@material-ui/core";
import DataTableView from "./DataTableView";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100vw",
      height: "100vh",
      background: theme.palette.type === "dark" ? "#152028" : "#ffffff",
    },
  })
);

const DataTableContainer = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Container>
        <DataTableView />
      </Container>
    </div>
  );
};

export default DataTableContainer;
