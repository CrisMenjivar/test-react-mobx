import React from "react";
import { observer } from "mobx-react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import CheckIcon from "@material-ui/icons/CheckCircle";
import User from "../../stores/users/User";
import { Order, RowTable } from "../../types";
import { getComparator, stableSort } from "../../utils";
import EnhancedTableHead from "./EnhancedTableHead";
import EnhancedTableToolbar from "./EnhancedTableToolbar";
import { Button } from "@material-ui/core";
import useContextUserStore from "../../hooks/useContextUserStore";
import CreateNewUserModal from "../CreateUser/CreateNewUserModal";
import DeleteDialog from "../Alerts/DeleteDialog";
import useContextAlertStore from "../../hooks/useContextAlertStore";
import { AlertMessage } from "../../stores/alerts";

const createRowFromUser = (user: User): RowTable => {
  return {
    accountId: user.accountId,
    userName: user.username,
    emailVerified: user.emailVerified,
    firstName: user.firstName,
    lastName: user.lastName,
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
    },
    paper: {
      width: "100%",
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    buttonAddNew: {
      marginTop: "100px",
      marginBottom: "20px",
    },
  })
);

const DataTableView = observer(() => {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<keyof RowTable>("accountId");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const { userStore } = useContextUserStore();
  const { alertStore } = useContextAlertStore();

  const { idsUserSelected } = userStore;
  const rows: RowTable[] = userStore.users.map((user) =>
    createRowFromUser(user)
  );

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof RowTable
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => `${n.accountId}`);

      userStore.idsUserSelected = newSelecteds;
      return;
    }
    userStore.idsUserSelected = [];
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    const selectedIndex = idsUserSelected.indexOf(name);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(idsUserSelected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(idsUserSelected.slice(1));
    } else if (selectedIndex === idsUserSelected.length - 1) {
      newSelected = newSelected.concat(idsUserSelected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        idsUserSelected.slice(0, selectedIndex),
        idsUserSelected.slice(selectedIndex + 1)
      );
    }

    userStore.idsUserSelected = newSelected;
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const deleteListUser = () => {
    userStore.deleteUsersByIds();
    const alert = new AlertMessage("Selection delete succefull!");
    alertStore.showMessage(alert);
  };

  const isSelected = (name: string) => idsUserSelected.indexOf(name) !== -1;

  return (
    <>
      <div className={classes.root}>
        <Button
          variant="contained"
          color="primary"
          className={classes.buttonAddNew}
          onClick={userStore.toggleModal}
        >
          Add new user
        </Button>

        <Paper className={classes.paper}>
          <EnhancedTableToolbar
            numSelected={idsUserSelected.length}
            deleteListUser={() => userStore.toggleDialogDelete()}
          />
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                numSelected={idsUserSelected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    const isItemSelected = isSelected(`${row.accountId}`);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                        hover
                        onClick={(event) =>
                          handleClick(event, `${row.accountId}`)
                        }
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={-1}
                        key={`${row.accountId}`}
                        selected={isItemSelected}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ "aria-labelledby": labelId }}
                          />
                        </TableCell>
                        <TableCell
                          component="th"
                          id={labelId}
                          scope="row"
                          padding="none"
                        >
                          {row.accountId}
                        </TableCell>
                        <TableCell align="right">{row.firstName}</TableCell>
                        <TableCell align="right">{row.lastName}</TableCell>
                        <TableCell align="right">{row.userName}</TableCell>
                        <TableCell align="right">
                          <CheckIcon />
                        </TableCell>
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      </div>

      <CreateNewUserModal />
      <DeleteDialog
        isOpen={userStore.isDialogDeleteConfirm}
        accept={deleteListUser}
        cancel={userStore.toggleDialogDelete}
      />
    </>
  );
});

export default DataTableView;
