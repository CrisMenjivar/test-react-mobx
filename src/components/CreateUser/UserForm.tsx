import React, { FunctionComponent } from "react";
import {
  Button,
  createStyles,
  FormControl,
  FormHelperText,
  Input,
  InputLabel,
  makeStyles,
  Theme,
} from "@material-ui/core";
import useContextUserStore from "../../hooks/useContextUserStore";
import User from "../../stores/users/User";
import { observer } from "mobx-react";
import FormGroup from "@material-ui/core/FormGroup";
import { ErrorApi } from "../../types";

import { FormikProvider, useFormik, useField, useFormikContext } from "formik";
import { object as YupObject, string as YupString } from "yup";
import useContextAlertStore from "../../hooks/useContextAlertStore";
import { AlertMessage } from "../../stores/alerts";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonSubmit: {
      marginTop: "50px",
    },
    error: {
      color: "#FF9494",
    },
  })
);

enum FieldsName {
  Username = "username",
  FirstName = "firstName",
  LastName = "lastName",
}

interface FieldTitleProps {
  fieldName: FieldsName;
  title: string;
  type?: string;
}

interface CreateUserParams {
  username: string;
  firstName: string;
  lastName: string;
}

const Field: FunctionComponent<FieldTitleProps> = ({
  fieldName,
  title,
  type = "text",
}) => {
  const classes = useStyles();

  const [{ value }, meta] = useField({ id: fieldName, name: fieldName });
  const {
    setFieldTouched,
    setFieldValue,
  } = useFormikContext<CreateUserParams>();

  const hasErrors = meta.touched && meta.error;

  const handleChange = (text: string) => {
    setFieldValue(fieldName, text);
  };

  return (
    <FormControl>
      <InputLabel htmlFor="my-input">{title}</InputLabel>
      <Input
        id="my-input"
        aria-describedby="my-helper-text"
        value={value}
        name="username"
        onChange={({ target }) => handleChange(target.value)}
        onBlur={() => setFieldTouched(fieldName)}
        type={type}
      />

      {hasErrors && (
        <FormHelperText id="my-helper-text" className={classes.error}>
          {hasErrors}
        </FormHelperText>
      )}
    </FormControl>
  );
};

const UserForm = observer(() => {
  const classes = useStyles();
  const { userStore } = useContextUserStore();
  const { alertStore } = useContextAlertStore();

  const validationSchema = YupObject({
    username: YupString()
      .required("The email is required")
      .email("The email must be must be valid"),
    firstName: YupString().required("Firt name is required"),
    lastName: YupString().required("Last name is required"),
  });

  const handlerSubmit = async (values: CreateUserParams) => {
    const user = new User(values.username, values.firstName, values.lastName);

    const response = await userStore.addUser(user);
    if ((response as User).accountId) {
      alertStore.showMessage(new AlertMessage("User add succefull"));
    } else {
      alert((response as ErrorApi).message);
    }
  };

  const formikProvider = useFormik<CreateUserParams>({
    initialValues: { username: "", firstName: "", lastName: "" },
    onSubmit: handlerSubmit,
    validationSchema,
    initialErrors: {
      username: "",
      firstName: "",
      lastName: "",
    },
  });

  return (
    <FormikProvider value={formikProvider}>
      <FormGroup onSubmit={formikProvider.submitForm}>
        <Field fieldName={FieldsName.Username} title="Email" type="email" />
        <Field fieldName={FieldsName.FirstName} title="Firt name" />
        <Field fieldName={FieldsName.LastName} title="Last name" />

        <Button
          variant="contained"
          color="primary"
          href="#contained-buttons"
          onClick={formikProvider.submitForm}
          type="submit"
          className={classes.buttonSubmit}
        >
          Create user
        </Button>
      </FormGroup>
    </FormikProvider>
  );
});

export default UserForm;
