import React from "react";
import {
  createStyles,
  Fade,
  IconButton,
  makeStyles,
  Modal,
  Theme,
} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import CloseRoundedIcon from "@material-ui/icons/CloseRounded";
import useContextUserStore from "../../hooks/useContextUserStore";
import { observer } from "mobx-react";
import UserForm from "./UserForm";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      minWidth: "300px",
    },
    titleModal: {
      color: "#ffffff",
      textAlign: "center",
    },
    buttonFloat: {
      position: "fixed",
      right: "20px",
      top: "20px",
    },
  })
);

const CreateNewUserModal = observer(() => {
  const classes = useStyles();
  const { userStore } = useContextUserStore();

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={userStore.isModalNewOpen}
      onClose={userStore.toggleModal}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={userStore.isModalNewOpen}>
        <div className={classes.paper}>
          <IconButton
            aria-label="delete"
            className={classes.buttonFloat}
            onClick={userStore.toggleModal}
          >
            <CloseRoundedIcon color="action" />
          </IconButton>

          <h2 id="transition-modal-title" className={classes.titleModal}>
            Create new user
          </h2>

          <UserForm />
        </div>
      </Fade>
    </Modal>
  );
});

export default CreateNewUserModal;
