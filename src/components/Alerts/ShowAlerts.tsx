import React, { useEffect } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import useContextAlertStore from "../../hooks/useContextAlertStore";
import { observer } from "mobx-react";
import { AlertMessage } from "../../stores/alerts";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      maxWidth: "400px",
      "& > * + *": {
        marginTop: theme.spacing(2),
      },
      position: "fixed",
      bottom: "30px",
      right: "30px",
    },
  })
);

interface AlertItemProps {
  alert: AlertMessage;
  deleteAlert: () => void;
}
const AlertItem = ({ alert, deleteAlert }: AlertItemProps) => {
  useEffect(() => {
    setTimeout(() => {
      deleteAlert();
    }, alert.duration);
    // eslint-disable-next-line
  }, []);

  return (
    <Alert severity={alert.severity} color={alert.color} onClick={deleteAlert}>
      {alert.message}
    </Alert>
  );
};

const ShowAlerts = observer(() => {
  const classes = useStyles();

  const { alertStore } = useContextAlertStore();

  if (alertStore.alerts.length <= 0) return null;

  return (
    <React.Fragment>
      <div className={classes.root}>
        {alertStore.alerts.map((alert) => (
          <AlertItem
            key={alert.id}
            alert={alert}
            deleteAlert={() => alertStore.deleteMessage(alert)}
          />
        ))}
      </div>
    </React.Fragment>
  );
});

export default ShowAlerts;
