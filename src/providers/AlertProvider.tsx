import { FunctionComponent } from "react";
import { AlertStore } from "../stores/alerts";
import AlertContext from "../context/AlertContext";

const AlertProvider: FunctionComponent = ({ children }) => {
  const alertStore = new AlertStore();
  return (
    <AlertContext.Provider
      value={{
        alertStore,
      }}
    >
      {children}
    </AlertContext.Provider>
  );
};

export default AlertProvider;
