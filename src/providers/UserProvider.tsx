import { FunctionComponent } from "react";
import { UserStore } from "../stores/users/UsersStore";
import UserContext from "../context/UserContext";

const UserProvider: FunctionComponent = ({ children }) => {
  const userStore = new UserStore();
  return (
    <UserContext.Provider
      value={{
        userStore,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserProvider;
