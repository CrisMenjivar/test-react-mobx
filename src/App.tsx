import { createMuiTheme, ThemeProvider } from "@material-ui/core";
import ShowAlerts from "./components/Alerts/ShowAlerts";
import DataTableContainer from "./components/UserDataTable/DataTableContainer";
import AlertProvider from "./providers/AlertProvider";
import UserPrivider from "./providers/UserProvider";

function App() {
  const theme = createMuiTheme({
    palette: {
      type: "dark",
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <UserPrivider>
        <AlertProvider>
          <DataTableContainer />
          <ShowAlerts />
        </AlertProvider>
      </UserPrivider>
    </ThemeProvider>
  );
}

export default App;
